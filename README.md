# Prerequisites
- Download and Unzip this repo
- Install [Docker](https://docs.docker.com/docker-for-windows/).
- (optional) Install an Editor like [Atom](https://atom.io/).


# Instructions
### View the Webpage
Double click the `index.html` to see the page.

### Learning HTML
Change the title to include your name.

### Learning CSS
Change the color from `orange` to `blue`.

### Learning Javascript
Turn on the Event Listener.

## Deploy with Docker (optional)
`docker build -t website .`
`docker run -d --name website -p 80:80 website`

## What's Next
Connect the Docker API you created from Lunch and Learn - REST API with your website in docker
