// add the welcome message
const welcome = document.getElementById("welcomeText");
welcome.innerHTML = "Welcome to myPage"
welcome.style.color = "red";

// set the logo to hidden at startup
const logo = document.getElementById("spaceeLogo");
logo.style.visibility = "hidden";

// listen to the button
const button = document.getElementById("clickMe");
button.addEventListener("click", function (event) {

  // check to see if the logo is already hidden
  if (logo.style.visibility === "hidden") {
    // if it's hidden change it to visible
    logo.style.visibility = "visible";
  } else {
    // if it's visible change it to hidden
    logo.style.visibility = "hidden";
  }
});
